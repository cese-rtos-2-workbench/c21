/*
 * Copyright (c) 2023 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author : Sebastian Bedin <sebabedin@gmail.com>
 */

/********************** inclusions *******************************************/

#include "main.h"
#include "cmsis_os.h"
#include "logger.h"
#include "dwt.h"
#include "board.h"

/********************** macros and definitions *******************************/

#define TASK_PERIOD_MS_         (1000)
#define TASK_TIMEOUT_MS_        (250)
#define MSG_TICK_               ("tick")

#define QUEUE_LENGTH_            (10)
#define QUEUE_ITEM_SIZE_         (sizeof(message_t))

/********************** internal data declaration ****************************/

typedef struct
{
    uint32_t value;
    TaskHandle_t task_handle;
} message_t;


/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static QueueHandle_t hqueue_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void task_high_(void *argument)
{
  message_t msg;
  while (true)
  {
    if (pdPASS == xQueueReceive(hqueue_, &msg, portMAX_DELAY))
    {
      LOGGER_LOG("process %d\n", msg.value);
      xTaskNotify(msg.task_handle, msg.value, eSetValueWithOverwrite);
    }
  }
}

static void task_low_(void *argument)
{
  uint32_t events;
  uint32_t value = 1;
  message_t msg;
  while (true)
  {
    msg.value = value;
    msg.task_handle = xTaskGetCurrentTaskHandle();

    xQueueSend(hqueue_, (void*)&msg, 0);

    events = ulTaskNotifyTake(pdTRUE, (TickType_t)(TASK_TIMEOUT_MS_ / portTICK_PERIOD_MS));
    if(0 < events)
    {
      if(value == events)
      {
        LOGGER_LOG("ok\n");
      }
      else
      {
        LOGGER_LOG("error\n");
      }
    }
    else
    {
      LOGGER_LOG("timeout\n");
    }

    vTaskDelay((TickType_t)(TASK_PERIOD_MS_ / portTICK_PERIOD_MS));

    value++;
  }
}

/********************** external functions definition ************************/

void app_init(void)
{
  LOGGER_LOG("tasks init\n");

  hqueue_ = xQueueCreate(QUEUE_LENGTH_, QUEUE_ITEM_SIZE_);
  while(NULL == hqueue_)
  {
    // error
  }

  BaseType_t status;

  status = xTaskCreate(task_high_, "task_high", 128, NULL, tskIDLE_PRIORITY + 1, NULL);
  while (pdPASS != status)
  {
    // error
  }

  status = xTaskCreate(task_low_, "task_low", 128, NULL, tskIDLE_PRIORITY, NULL);
  while (pdPASS != status)
  {
    // error
  }


  cycle_counter_init();
}

/********************** end of file ******************************************/
